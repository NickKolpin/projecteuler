"""
Find the sum of all multiples of 3 or 5 below 1000
"""

vals = []
x = 3
while x < 1000:
    vals.append(x)
    x += 3
x = 5
while x < 1000:
    if x not in vals:
        vals.append(x)
    x += 5
print sum(vals)
