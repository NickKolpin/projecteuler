"""
if n_i is odd:
    n_(i+1) = 3n_i * 1
else:
    n_(i+1) = n / 2

Sequence always reaches 1

Which N_0 < 1e6 has the longest sequence length
"""

def get_lengths(target, lengths):
    if target == 1:
        return 1
    if target in lengths:
        return lengths[target]
    seq = [target]
    while target > 1:
        if target % 2 == 0:
            target /= 2
        else:
            target = 3 * target + 1
        if target in lengths:
            l = lengths[target]
            while len(seq) > 0:
                l += 1
                lengths[seq[-1]] = l
                seq = seq[:-1]
            return l
        seq.append(target)
    l = 0
    while len(seq) > 0:
        l += 1
        lengths[seq[-1]] = l
        seq = seq[:-1]
    return l

def maxlength(maxtar):
    lengths = {}
    lmax = 0
    tmax = 0
    for i in range(1, maxtar):
        l = get_lengths(i, lengths)
        if l > lmax:
            lmax = l
            tmax = i
            print "%d has length %d" % (i, l)
    return tmax, lmax


if __name__ == "__main__":
    """
    lengths = {}
    t = 13
    l = get_lengths(t, lengths)
    print "seq(%d) has length %d" % (t, l)
    keys = lengths.keys()
    keys.sort()
    for key in keys:
        print "\t%d -> %d" % (key, lengths[key])
"""
    tmax, lmax = maxlength(int(1e6))
    print "max length of %d from %d" % (lmax, tmax)



