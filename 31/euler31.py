"""
Currency made of 1p, 2p, 5p, 10p, 20p, 50p, 100p, 200p

How may ways can you make up 200p?
"""

# Since 1 is allowed, any value reached below target is possible
coins = [1, 2, 5, 10, 20, 50, 100, 200]

def count(target, allowed):
    print target, allowed
    allowed.sort(reverse=True)
    if target <= 0:
        return 0
    n = len(allowed)
    if n == 0:
        return 0
    elif n == 1:
        if target % allowed[0] == 0:
            return 1
        else:
            return 0
    nmax = target / allowed[0]
    print "nmax = %d" % nmax
    c = 0
    for i in range(nmax+1):
        newtarget = target - i * allowed[0]
        if newtarget > 0:
            c += count(newtarget, allowed[1:])
        elif newtarget == 0:
            c += 1
    return c

if __name__ == "__main__":
    #c = count(5, [5, 2, 1])
    c = count(200, coins)
    print "c = %d" % c
