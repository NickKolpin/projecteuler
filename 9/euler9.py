"""
There exists exactly one pythagorean triplet a^2 + b^2 = c^2 for which a + b + c is 1000.

Find a * b * c
"""

# Know a < b < c
# For a given c: a + b = 1000 - c, a < b
# 1 <= a <= b, so  a < b < 1000 - c - 1

def istriplet(a, b, c):
    return a**2 + b**2 == c**2

def check(c):
    for b in range(2, 1000 - c):
        a = 1000 - c - b
        #print "%d + %d + %d = %d" % (a, b, c, a + b + c)
        if istriplet(a, b, c):
            prod = a * b * c
            print "%d^2 + %d^2 = %d^2, %d * %d * %d = %d" % (a, b, c, a, b, c, prod)
            return True
    return False

for c in range(333, 998):
    check(c)

                

